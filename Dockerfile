FROM node:16

WORKDIR /app

COPY package*.json ./

# install dependencies
RUN npm install next react react-dom --verbose

# copy source files
COPY . ./

# build
RUN npm run build

EXPOSE 3000

CMD [ "npm", "start" ]


